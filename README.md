# twitter-scraper


Per utilizzare lo scraper è sufficiente modificare il file *main.py* collocato in

```cmd
src/datalab_scraper/main.py
```

Nello specifico, alla riga 52 potranno essere inseriti:
1. l'hashtag da scrapare
2. la lingua
3. data di inizio del periodo di scraping
4. data di fine del periodo di scraping

Per esempio:

```python
twitterscraper = TwitterScraper(token=get_twitter_api_token(), search="#musk", lang="en", startTime='2023-06-02', finalTime='2023-06-05')
```


è inoltre necessario aggiungere, nella directory principale del progetto, un file .env contenente il token dell'API Tweepy.

