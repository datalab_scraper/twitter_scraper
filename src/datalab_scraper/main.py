import pandas as pd
import torch
from src.datalab_scraper.hashtag_scraper import TwitterScraper
from src.datalab_core.normalizers.twitter_api import ScraperSchemaNormalizer

from src.datalab_core.processors.agreement import AgreementExtractor
from src.datalab_core.processors.edge_betweennes import BetweennessExtractor
from src.datalab_core.processors.communities import CommunitiesExtractor
from src.datalab_core.processors.keyword import KeywordExtractor

from src.datalab_core.analyzers.statistical_analyses import StatisticalAnalyzer
from src.datalab_core.analyzers.descriptive_analyses import DescripitiveAnalyzer

from dotenv import load_dotenv
import os
import pathlib


def get_twitter_api_token():
    load_dotenv()
    token = os.getenv('token')
    return token


if __name__ == '__main__':

    scraped_path = pathlib.Path('./datasets/scraping_and_processing/scraped')
    scraped_path.mkdir(parents=True, exist_ok=True)

    normalized_path = pathlib.Path('./datasets/scraping_and_processing/normalized')
    normalized_path.mkdir(parents=True, exist_ok=True)

    agreement_path = pathlib.Path('./datasets/scraping_and_processing/agreement')
    agreement_path.mkdir(parents=True, exist_ok=True)

    keywords_path = pathlib.Path('./datasets/scraping_and_processing/keywords')
    keywords_path.mkdir(parents=True, exist_ok=True)

    betweenness_path = pathlib.Path('./datasets/scraping_and_processing/betweenness')
    betweenness_path.mkdir(parents=True, exist_ok=True)

    communities_path = pathlib.Path('./datasets/scraping_and_processing/communities')
    communities_path.mkdir(parents=True, exist_ok=True)

    statistical_analyses_path = pathlib.Path('./datasets/statistical_analyses/plots')
    statistical_analyses_path.mkdir(parents=True, exist_ok=True)

    descriptive_analyses_path = pathlib.Path('./datasets/descriptive_analyses/plots')
    descriptive_analyses_path.mkdir(parents=True, exist_ok=True)


    twitterscraper = TwitterScraper(token=get_twitter_api_token(), search="#musk", lang="en", startTime='2023-06-02', finalTime='2023-06-05')
    df = twitterscraper.scrape()
    df = twitterscraper.parallelize_original_tweet_scraping(df)
    filename_df = ''.join(filter(str.isalnum, twitterscraper.search))
    df.to_json(f"{scraped_path}/{filename_df}_scraped.json", force_ascii=False)

    df = ScraperSchemaNormalizer().normalize(df)
    df.to_json(f'{normalized_path}/{filename_df}_normalized.json', force_ascii=False)

    roberta = torch.hub.load('pytorch/fairseq', 'roberta.large.mnli')
    agreement = AgreementExtractor(roberta)
    df = agreement.compute_agreement(df)
    df.to_json(f'{agreement_path}/{filename_df}_agreement.json', force_ascii=False)

    df = KeywordExtractor().extract_kw_yake(df)
    df.to_json(f'{keywords_path}/{filename_df}_keywords.json', force_ascii=False)

    df = BetweennessExtractor().compute_edge_betweenness(df)
    df.to_json(f'{betweenness_path}/{filename_df}_betweenness.json', force_ascii=False)

    comm_extractor = CommunitiesExtractor(df)
    comm_extractor.visualize_resolution()
    df.to_json(f'{communities_path}/{filename_df}_communities.json', force_ascii=False)

    StatisticalAnalyzer.generate_report()
    DescripitiveAnalyzer.generate_report()
