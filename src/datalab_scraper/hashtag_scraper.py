import numpy as np
import tweepy
import pandas as pd
import dateutil.parser
import time
from multiprocessing.pool import ThreadPool
import snscrape.modules.twitter as sntwitter



class TwitterScraper:

    def __init__(self, token, search, lang, startTime, finalTime):
        self.token = token
        self.search = search
        self.lang = lang
        self.startTime = startTime
        self.finalTime = finalTime


    def scrape(self):

        Client = tweepy.Client(bearer_token=self.token)

        self.startTime += 'T00:00:00.000Z'
        self.finalTime += 'T00:00:00.000Z'
        startTime = dateutil.parser.isoparse(self.startTime)
        finalTime = dateutil.parser.isoparse(self.finalTime)
        lastTime = finalTime

        tweetAll = pd.DataFrame(columns=['created_at', 'edit_history_tweet_ids', 'text',
                                         'author_id', 'public_metrics', 'id', 'referenced_tweets',
                                         'in_reply_to_user_id'])

        nameAcc = f'{self.search} lang:{self.lang}'

        while lastTime > startTime:
            try:
                search = Client.search_all_tweets(f'{nameAcc} -is:retweet',
                                                  tweet_fields=['created_at', 'author_id', 'in_reply_to_user_id',
                                                                'public_metrics', 'referenced_tweets'], max_results=400,
                                                  end_time=lastTime, start_time=startTime)
                frame = pd.DataFrame([i[1].data for i in enumerate(search[0])])
                lastTime = dateutil.parser.isoparse(frame.loc[len(frame) - 1, 'created_at'])
                tweetAll = pd.concat([tweetAll, frame])
                print(lastTime)
            except:
                pass
            if search[0] == None:
                break
            time.sleep(5)

        tweetAll['in_reply_to_user_id'] = tweetAll['in_reply_to_user_id'].astype('str')
        return tweetAll



    @staticmethod
    def _extract_reply_tweet_id(df: pd.DataFrame) -> pd.DataFrame:
        df = df[df.referenced_tweets.notna()].reset_index(drop=True)
        df['inReplyToTweetId'] = df.referenced_tweets.apply(lambda x: x[0]['id'])
        df = df[df.inReplyToTweetId.notna()].reset_index(drop=True)
        return df


    @staticmethod
    def _scrapa_tweet_originali(reply_tweet_id):

        originali = []

        display_tweet = np.random.choice(["Yes", "No"], p=[0.01, 0.99])
        if display_tweet == 'Yes':
            print(reply_tweet_id)

        try:
            for i in sntwitter.TwitterTweetScraper(reply_tweet_id,
                                                   mode=sntwitter.TwitterTweetScraperMode.SINGLE).get_items():
                originali.append([reply_tweet_id, i.rawContent, i.user.username])
        except:
            originali.append(['NO_ID', 'NO_CONTENT', 'NO_USERNAME'])

        originali = pd.DataFrame(originali,
                                 columns=['inReplyToTweetId', 'originalTweetContent', 'originalUsernamePost'])
        return originali


    @staticmethod
    def parallelize_original_tweet_scraping(df:pd.DataFrame) -> pd.DataFrame:
        pool = ThreadPool()
        df = TwitterScraper._extract_reply_tweet_id(df)
        lista = df.inReplyToTweetId.tolist()
        original_tweets = pool.map(TwitterScraper._scrapa_tweet_originali, lista)
        original_tweets_df = pd.concat(original_tweets)
        df_merged = pd.merge(df, original_tweets_df)

        return df_merged




