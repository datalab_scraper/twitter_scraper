from src.datalab_core.scraper_schema import ScraperSchema
import pandas as pd

class ScraperSchemaNormalizer:
    def normalize(self, df_in: pd.DataFrame) -> pd.DataFrame:
        df_out = ScraperSchema.create_dataframe()

        df_out[ScraperSchema.TEXT] = df_in['text']
        df_out[ScraperSchema.USERNAME] = "U" + df_in['author_id'].astype('str')
        df_out[ScraperSchema.ORIGINALUSERNAMEPOST] = "U" + df_in['in_reply_to_user_id'].astype('str')
        df_out[ScraperSchema.ORIGINALTWEETCONTENT] = df_in['originalTweetContent']
        df_out = df_out[df_out['originalUsernamePost'] != 'Unan']  # happens when forcing the dtype + adding 'U'
        df_out = df_out.reset_index(drop=True)
        return df_out
