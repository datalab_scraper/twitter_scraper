import pandas as pd
import os
import networkx as nx
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt


class DescripitiveAnalyzer:
    def __init__(self, df):
        self.df = df.copy()
        self.G = nx.from_pandas_edgelist(df, 'username', 'originalUsernamePost', create_using=nx.DiGraph)
        self.top10comms = (df.TargetModularity.value_counts()[:10] + \
                           df.SourceModularity.value_counts()[:10]) \
            .sort_values(ascending=False).index.tolist()

    def component_sizes(self, comp_func):
        """Restituisce:
        Dimensione delle 2 componenti connesse deboli più grandi (in nodi e edge) e
        Dimensione delle 2 componenti forti più grandi (in nodi ed edge)"""
        G = self.G
        components = list(comp_func(G))
        components.sort(key=len, reverse=True)

        sizes = []
        for comp in components[:2]:
            subgraph = G.subgraph(comp)
            sizes.append((len(comp), subgraph.number_of_edges()))

        return sizes

    def return_size_top10comms(self):
        number_of_nodes_top10comms = []
        number_of_edges_top10comms = []
        for comm in self.top10comms:
            sub_df = self.df[(self.df['SourceModularity'] == comm) & (self.df['TargetModularity'] == comm)]
            sub_g = nx.from_pandas_edgelist(sub_df, 'username', 'originalUsernamePost', create_using=nx.DiGraph)
            number_of_nodes_top10comms.append(f"Comunità {comm}: {sub_g.number_of_nodes()}")
            number_of_edges_top10comms.append(f"Comunità {comm}: {sub_g.number_of_edges()}")

        return number_of_nodes_top10comms, number_of_edges_top10comms

    def plot_degree_distribution(self, title):
        G = self.G
        degree_freq = nx.degree_histogram(G)
        degrees = range(len(degree_freq))
        plt.loglog(degrees, degree_freq, 'go-')
        plt.xlabel('Degree')
        plt.ylabel('Frequency')
        plt.title(f"Degree distribution for {title}")
        plt.savefig(f'./datasets/descriptive_analyses/plots/{title}_degree_distribution.png', bbox_inches='tight')
        plt.show()

    def return_statistics(self):
        G = self.G

        number_of_nodes = G.number_of_nodes()
        number_of_edges = G.number_of_edges()
        top_5_indegree = sorted(self.G.in_degree, key=lambda x: x[1], reverse=True)[:5]
        top_5_outdegree = sorted(self.G.out_degree, key=lambda x: x[1], reverse=True)[:5]

        top2_weakly_connected_comps = self.component_sizes(nx.weakly_connected_components)
        top2_strongly_connected_comps = self.component_sizes(nx.strongly_connected_components)

        number_of_nodes_top10comms = self.return_size_top10comms()[0]
        number_of_edges_top10comms = self.return_size_top10comms()[1]

        report_df = pd.DataFrame()
        report_df['total_number_of_nodes'] = [number_of_nodes]
        report_df['total_number_of_edges'] = [number_of_edges]

        report_df['top_5_indegree'] = [top_5_indegree]
        report_df['top_5_outdegree'] = [top_5_outdegree]

        report_df['size_top2_weakly_connected_comps'] = [top2_weakly_connected_comps]
        report_df['size_top2_strongly_connected_comps'] = [top2_strongly_connected_comps]

        report_df['number_of_nodes_top10comms'] = [number_of_nodes_top10comms]
        report_df['number_of_edges_top10comms'] = [number_of_edges_top10comms]

        return report_df

    @staticmethod
    def generate_report():

        PATH = r'./datasets/scraping_and_processing/communities'

        dfs = {}

        for file in os.listdir(PATH):
            if file.endswith('.json'):
                dataset_name = file.split('_')[0]

                df = pd.read_json(f"{PATH}/{file}")

                dfs[dataset_name] = df

        reports = []
        for df in dfs.keys():
            da = DescripitiveAnalyzer(dfs[df])
            report = da.return_statistics()
            da.plot_degree_distribution(df)
            report = report.rename(index={0:df})
            reports.append(report)

        descriptive_report = pd.concat(reports)
        descriptive_report.to_excel('./datasets/descriptive_analyses/descriptive_analyses_report.xlsx')