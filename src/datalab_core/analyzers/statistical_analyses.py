import pandas as pd
import os
import networkx as nx
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt



class StatisticalAnalyzer:

    def __init__(self, df):
        self.df = df.copy()
        self.top10comms = (df.TargetModularity.value_counts()[:10] + \
                           df.SourceModularity.value_counts()[:10]) \
            .sort_values(ascending=False).index.tolist()

    @staticmethod
    def compute_chi2(column_x, column_y):
        return stats.chi2_contingency(pd.crosstab(column_x, column_y)).pvalue

    def same_comm_source_and_target_vs_agreement(self):
        """ confronto solo fra due variabili: agreement e indicatore del fatto che
        source e target sono nella stessa comunità"""

        df = self.df.copy()
        df['SameCommunitySourceTarget'] = (df['SourceModularity'] == df['TargetModularity']).astype('int')
        return df

    def intra_comm_and_small_comms_are_11_vs_agreement(self):
        """ Prendendo solo gli edge interni alle comunità, chi quadro fra le diverse comunità,
        aggregando in un unico bin le comunità piccole dopo la 10, di nuovo,
        per evitare troppi sample piccoli. Dunque, tutte le comunità dopo la 10 vengono
        sovrascritte con il numero 11"""

        df = self.df.copy()
        top10comms = self.top10comms

        df.loc[~df['SourceModularity'].isin(top10comms), 'SourceModularity'] = 1_000_000
        df.loc[~df['TargetModularity'].isin(top10comms), 'TargetModularity'] = 1_000_000
        df = df[df['SourceModularity'] == df['TargetModularity']].reset_index(drop=True)
        return df

    def inter_comm_and_small_comms_are_11_vs_agreement(self):

        df = self.df.copy()
        top10comms = self.top10comms

        df.loc[~df['SourceModularity'].isin(top10comms), 'SourceModularity'] = 1_000_000
        df.loc[~df['TargetModularity'].isin(top10comms), 'TargetModularity'] = 1_000_000
        df = df[df['SourceModularity'] != df['TargetModularity']].reset_index(drop=True)
        df['InterCommunities'] = df['SourceModularity'].astype('str') + '-' + df['TargetModularity'].astype('str')
        return df

    def inter_comm_source_and_target_vs_agreement(self):
        df = self.df.copy()
        top10comms = self.top10comms

        df = pd.concat(
            [
                df.loc[(df['SourceModularity'] == df['TargetModularity']) &
                       (df['SourceModularity'].isin(top10comms))][
                    ['SourceModularity', 'TargetModularity', 'agreement']],

                df.loc[(df['SourceModularity'].isin(top10comms)) &
                       (df['SourceModularity'] != df['TargetModularity'])][
                    ['SourceModularity', 'TargetModularity', 'agreement']
                ]
            ]
        )
        df['InterCommunities'] = df['SourceModularity'].astype('str') + '-' + df['TargetModularity'].astype('str')
        return df

    def top_100_kw_vs_agreement(self):
        """ Confronto tra keyword e agreement sulle 100 keyword più frequenti in tutto il dataset """

        df = self.df.copy()
        df = df.groupby(['TopKeyword', 'agreement']).size().unstack(fill_value=0).reset_index()
        df['sum'] = df[[-1, 1]].sum(axis=1)
        df = df.sort_values('sum', ascending=False).reset_index(drop=True).drop(['sum'], axis=1)
        df = df.head(100)
        df = df.set_index('TopKeyword')
        return df

    def inter_top_10_comms_kw_vs_agreement(self):
        """Confronto tra keyword e agreement all'interno di ciascuna delle prime 10 comunità"""
        df = self.df.copy()
        top10comms = self.top10comms

        comms_and_pvalues = []
        for i in top10comms:
            kw = df.loc[(df['SourceModularity'] == i) &
                        (df['TargetModularity'] == i)].groupby(['TopKeyword', 'agreement']).size().unstack(fill_value=0)
            try:
                kw['sum'] = kw[[-1, 1]].sum(axis=1)
                kw = kw.sort_values('sum', ascending=False).head(10)
                kw = kw[[-1, 1]]
                comms_and_pvalues.append(f"Comunità {i}-{i} {stats.chi2_contingency(kw)[1]}")
            except:
                comms_and_pvalues.append(f"Comunità {i}-{i} impossibile runnare il test")
        return comms_and_pvalues

    def whole_dataset_kw_vs_agreement(self):
        """Confronto, su tutto il dataset, fra keyword che lega comunità diverse e agreement"""
        df = self.df.copy()

        df = df.loc[(df['SourceModularity'] != df['TargetModularity'])] \
            .assign(
            SKT=df['SourceModularity'].astype('str') + '-' + df['TopKeyword'] + '-' + df['TargetModularity'].astype(
                'str'))

        kw = pd.crosstab(df['SKT'], df['agreement'])
        kw['somma'] = kw[[-1, 1]].sum(axis=1)
        kw = kw.sort_values('somma', ascending=False)[[-1, 1]]

        return kw

    def top_10_comms_source_notequals_target_kw_vs_agreement(self):
        """Confronto fra keyword che lega comunità diverse fra le prime 10 e agreement"""
        df = self.df.copy()
        top10comms = self.top10comms

        df = df.loc[(df['SourceModularity'] != df['TargetModularity']) & (df['agreement'] != 'Error')] \
            .assign(
            SKT=df['SourceModularity'].astype('str') + '-' + df['TopKeyword'] + '-' + df['TargetModularity'].astype(
                'str'))

        df = df.loc[(df['SourceModularity'].isin(top10comms)) &
                    (df['TargetModularity'].isin(top10comms)) &
                    (df['SourceModularity'] != df['TargetModularity'])]
        return df

    def top_10_comms_source_notequals_target_top_10_kw_vs_agreement(self):
        """ Confronto fra le top 10 keywords che legano comunità diverse fra le prime 10 e agreement """
        df = self.df.copy()
        top10comms = self.top10comms

        df = df.loc[(df['SourceModularity'] != df['TargetModularity']) & (df['agreement'] != 'Error')] \
            .assign(
            SKT=df['SourceModularity'].astype('str') + '-' + df['TopKeyword'] + '-' + df['TargetModularity'].astype(
                'str'))

        df = df.loc[(df['SourceModularity'].isin(top10comms)) &
                    (df['TargetModularity'].isin(top10comms)) &
                    (df['SourceModularity'] != df['TargetModularity'])][['SKT', 'agreement']]

        dfs = []
        for comm in top10comms:
            dfs.append(df[df.SKT.str.startswith(f"{comm}-")].head(10))
        dfs = pd.concat(dfs)

        return dfs

    def kw_top10comms_and_whole_dataset_vs_agreement(self):
        """ Confronto fra keyword che lega una delle prime 10 comunità con il resto del dataset e agreement"""
        df = self.df.copy()
        top10comms = self.top10comms

        df['SKT'] = df['SourceModularity'].astype('str') + '-' + df['TopKeyword'] + '-' + df['TargetModularity'].astype(
            'str')

        df = pd.crosstab(
            df.loc[(df['SourceModularity'].isin(top10comms)) & (~df['TargetModularity'].isin(top10comms))]['SKT'],
            df.loc[(df['SourceModularity'].isin(top10comms)) & (~df['TargetModularity'].isin(top10comms))]['agreement'])

        df['somma'] = df[[-1, 1]].sum(axis=1)
        df = df.loc[(df != 0).any(axis=1)].sort_values('somma', ascending=False).drop('somma', axis=1)
        return df

    def intra_top_10_comms_kruskal(self):
        df = self.df.copy()
        top10comms = self.top10comms

        comms_and_kruskal = []

        for i in top10comms:
            temp = df.loc[(df['SourceModularity'] == i) & (df['TargetModularity'] == i)]
            edge_bet_agreement = temp.loc[temp['agreement'] == 1]['edge_bet']
            edge_bet_disagreement = temp.loc[temp['agreement'] == -1]['edge_bet']
            try:
                comms_and_kruskal.append(
                    f"Comunità {i}-{i} {stats.kruskal(edge_bet_agreement.values, edge_bet_disagreement.values).pvalue}")
            except:
                comms_and_kruskal.append(f"{i} All numbers are identical in kruskal")

        return comms_and_kruskal

    def top_10_comms_kruskal(self):
        df = self.df.copy()
        top10comms = self.top10comms

        df = df.loc[(df['SourceModularity'].isin(top10comms)) & (~df['TargetModularity'].isin(top10comms))]
        edge_bet_agreement = df.loc[df['agreement'] == 1]['edge_bet']
        edge_bet_disagreement = df.loc[df['agreement'] == -1]['edge_bet']
        return stats.kruskal(edge_bet_agreement.values, edge_bet_disagreement.astype('int').values).pvalue

    def kruskal_whole_dataset_small_comms_are_999(self):
        df = self.df.copy()
        edge_bet_agreement = df.loc[df['agreement'] == 1]['edge_bet']
        edge_bet_disagreement = df.loc[df['agreement'] == -1]['edge_bet']
        return stats.kruskal(edge_bet_agreement.values, edge_bet_disagreement.values).pvalue

    def plot_kruskal_agreement(self, title):
        df = self.df.copy()

        edges_agreement = df.loc[df.agreement == 1].edge_bet.values
        edges_disagreement = df.loc[df.agreement == -1].edge_bet.values

        percs = np.linspace(0, 100, 500)

        qn_edges_agreement = np.percentile(edges_agreement, percs)
        qn_edges_disagreement = np.percentile(edges_disagreement, percs)

        plt.plot(percs[:], qn_edges_agreement[:])
        plt.plot(percs[:], qn_edges_disagreement[:])
        plt.title(f"Kruskal wallis plot for {title}. Orange line: disagreement; blue: agreement")
        plt.yscale('log')
        plt.savefig(f'./datasets/statistical_analyses/plots/{title}_degree_distribution.png', bbox_inches='tight')
        plt.show()



    @staticmethod
    def generate_report():

        PATH = r'./datasets/scraping_and_processing/communities'

        dfs = {}

        for file in os.listdir(PATH):
            if file.endswith('.json'):
                dataset_name = file.split('_')[0]

                df = pd.read_json(f"{PATH}/{file}")

                dfs[dataset_name] = df

        report = []

        for i in dfs.keys():
            if i == 'blessed':
                pass
            else:
                print(i, '\n')

                report_page = []
                report_page.append(i)

                sa = StatisticalAnalyzer(dfs[i])

                a1 = sa.same_comm_source_and_target_vs_agreement()
                report_page.append(sa.compute_chi2(a1['SameCommunitySourceTarget'], a1['agreement']))

                a2 = sa.intra_comm_and_small_comms_are_11_vs_agreement()
                report_page.append(sa.compute_chi2(a2['SourceModularity'], a1['agreement']))

                try:
                    a3 = sa.inter_comm_and_small_comms_are_11_vs_agreement()
                    report_page.append(sa.compute_chi2(a3['InterCommunities'], a3['agreement']))
                except:
                    report_page.append('Error when performing A3')

                a4 = sa.inter_comm_source_and_target_vs_agreement()
                report_page.append(sa.compute_chi2(a4['InterCommunities'], a4['agreement']))

                a5 = sa.top_100_kw_vs_agreement()
                report_page.append(stats.chi2_contingency(a5).pvalue)

                a6 = sa.inter_top_10_comms_kw_vs_agreement()
                report_page.append(a6)

                a7 = sa.whole_dataset_kw_vs_agreement()
                report_page.append(stats.chi2_contingency(a7).pvalue)

                a8 = sa.top_10_comms_source_notequals_target_kw_vs_agreement()
                report_page.append(sa.compute_chi2(a8['SKT'], a8['agreement']))

                a9 = sa.top_10_comms_source_notequals_target_top_10_kw_vs_agreement()
                report_page.append(sa.compute_chi2(a9['SKT'], a9['agreement']))

                a10 = sa.kw_top10comms_and_whole_dataset_vs_agreement()
                report_page.append(stats.chi2_contingency(a10).pvalue)

                a11 = sa.intra_top_10_comms_kruskal()
                report_page.append(a11)
                try:
                    a12 = sa.top_10_comms_kruskal()
                    report_page.append(a12)
                except ValueError as e:
                    if str(e) == 'All numbers are identical in kruskal':
                        report_page.append(str(e))

                a13 = sa.kruskal_whole_dataset_small_comms_are_999()
                report_page.append(a13)

                report.append(report_page)

                sa.plot_kruskal_agreement(i)

        report = pd.DataFrame(report, columns=['name'] + [f"a{i}" for i in range(1,13+1)])
        report.to_excel('./datasets/statistical_analyses/statistical_analyses_report.xlsx', index=False)
