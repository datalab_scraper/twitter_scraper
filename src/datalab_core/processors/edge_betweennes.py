from igraph import Graph as IGraph



class BetweennessExtractor:

    @staticmethod
    def compute_edge_betweenness(df):
        G = IGraph.TupleList(df[['username','originalUsernamePost']].itertuples(index=False), directed=True)
        bet = G.edge_betweenness()

        dfbet = df.copy()
        dfbet['edge_bet'] = bet

        return dfbet
