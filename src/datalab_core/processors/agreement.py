


class AgreementExtractor:
    def __init__(self, roberta):
        self.roberta = roberta

    def compute_agreement(self, df):
        agreements = []

        for i in df.iterrows():
            print(i[0])
            if i[0] % 100 == 0:
                print(i[0])

            tokens = self.roberta.encode(i[1]['text'], i[1]['originalTweetContent'])
            try:
                agreements.append(self.roberta.predict('mnli', tokens).argmax())  # 0: contradiction
            except:
                agreements.append('Error')

        df['agreement'] = agreements
        df = df[df['agreement']!='Error'].reset_index(drop=True)
        df['agreement'] = df['agreement'].apply(AgreementExtractor._estrai_agreement)
        df['agreement'] = df['agreement'].apply(AgreementExtractor._rimappa_valori)
        return df


    @staticmethod
    def _estrai_agreement(x):
        try:
            return x.item()
        except:
            return x


    @staticmethod
    def _rimappa_valori(x):
        """ 0: contradiction      -1
            1 neutral             +1
            2: entailment.        +1 """
        if x == 0:
            x = -1
            return x
        elif x == 1 or x == 2:
            x = 1
            return x
        else:
            return x