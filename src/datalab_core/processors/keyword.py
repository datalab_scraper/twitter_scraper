import yake


class KeywordExtractor:

    @staticmethod
    def extract_kw_yake(df):
        language = 'en'
        max_ngram_size = 2
        numOfKeywords = 1
        custom_kw_extractor = yake.KeywordExtractor(lan=language, n=max_ngram_size,
                                                    top=numOfKeywords, features=None)

        extract_keywords = lambda x: [k[0] for k in custom_kw_extractor.extract_keywords(x)]
        df['TopKeyword'] = df['text'].apply(extract_keywords)
        try:
            df['TopKeyword'] = df['TopKeyword'].apply(lambda x: x[0])
        except:pass

        df['TopKeyword'] = df.apply(KeywordExtractor._rimuovi_prima_parola, axis=1)
        df = df[df['TopKeyword'] != ''].reset_index(drop=True)
        df.TopKeyword = df.TopKeyword.str.lower()
        return df


    @staticmethod
    def _rimuovi_prima_parola(x):
        if x['originalUsernamePost'] == x['TopKeyword']:
            return "".join(x['TopKeyword'].split()[1:])
        else:
            return x['TopKeyword']







