from itertools import chain
import networkx as nx
from networkx.algorithms.community import greedy_modularity_communities
import matplotlib.pyplot as plt



class CommunitiesExtractor:
    def __init__(self, df):
        self.df = df
        self.G = nx.from_pandas_edgelist(df, "username", "originalUsernamePost", create_using=nx.DiGraph)

    def visualize_resolution(self):
        while True:
            resolution = float(input('Inserisci il valore del parametro resolution (e.g. 1.2) per visualizzare la distribuzione delle top 10 comunità '))
            c = greedy_modularity_communities(self.G, resolution=resolution)
            a = []

            for indice in range(len(c)):
                a.append(dict(zip(list(c[indice]), (f'{str(indice)} ' * len(c[indice])).split(' '))))


            b = dict(chain(*map(dict.items, a)))

            self.df['SourceModularity'] = self.df["username"].map(b)
            self.df['TargetModularity'] = self.df["originalUsernamePost"].map(b)

            (self.df.TargetModularity.value_counts()[:10] + \
             self.df.SourceModularity.value_counts()[:10]).sort_values(ascending=False).plot(kind='bar')
            plt.title(f'resolution {resolution}')
            plt.show()

            choice = input("Vuoi provare con un valore diverso per la resolution? (y / n). Se inserisci n, verrà selezionato l'ultimo valore che hai scelto ")
            if choice == 'n':
                CommunitiesExtractor.pick_best_resolution(self.G, self.df, resolution)
                break
            elif choice == 'y':
                continue


    @staticmethod
    def pick_best_resolution(G, df, resolution):
        c = greedy_modularity_communities(G, resolution=resolution)
        a = []

        for indice in range(len(c)):
            a.append(dict(zip(list(c[indice]), (f'{str(indice)} ' * len(c[indice])).split(' '))))

        b = dict(chain(*map(dict.items, a)))

        df['SourceModularity'] = df["username"].map(b)
        df['TargetModularity'] = df["originalUsernamePost"].map(b)

        return df

