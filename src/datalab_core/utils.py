def converti_in_int(x):
    try:
        return x.item()
    except:
        return x


def rimappa_valori(x):
    """ 0: contradiction      -1
        1 neutral             +1
        2: entailment.        +1 """
    if x == 0:
        x = -1
        return x
    elif x == 1 or x == 2:
        x = 1
        return x
    else:
        return x


def parallelizza_agreement(coppia):
    valori = []

    originaltweet = coppia
    texttweet = original_text[coppia]

    tokens = roberta.encode(texttweet, originaltweet)
    try:
        valori.append({coppia: roberta.predict('mnli', tokens).argmax()})  # 0: contradiction
    except:
        valori.append({coppia: 'Error'})

    return valori


def mappa_agreement(df: pd.DataFrame, roberta):
    valori = []
    for i in df.iterrows():
        if i[0] % 100 == 0:
            print(i[0])

        tokens = roberta.encode(i[1]['text'], i[1]['originalTweetContent'])
        try:
            valori.append(roberta.predict('mnli', tokens).argmax())  # 0: contradiction
        except:
            valori.append('Error')
    return valori




