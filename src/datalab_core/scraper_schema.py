import pandas as pd


class ScraperSchema:
    TEXT = 'text'
    USERNAME = 'username'
    ORIGINALUSERNAMEPOST = 'originalUsernamePost'
    ORIGINALTWEETCONTENT = 'originalTweetContent'


    schema = {
        TEXT: str,
        USERNAME: str,
        ORIGINALUSERNAMEPOST: str,
        ORIGINALTWEETCONTENT: str,
    }

    @staticmethod
    def create_dataframe():
        return pd.DataFrame(columns=ScraperSchema.schema.keys()).astype(ScraperSchema.schema)
